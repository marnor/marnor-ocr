var fs = require('fs');
var AdmZip = require('adm-zip');
var mkdirp = require('mkdirp');
var Promise = require('bluebird');

function isValidEntry(zipEntry) {
	if(zipEntry.isDirectory) return false;
	if(zipEntry.entryName !== zipEntry.name) return false;
	return true;
}

var FileHelper = {
	
	createDir: function(path) {
		return Promise.promisify(mkdirp)(path);
	},
	
	unzip: function(filePath, destinationPath) {
		var paths = {};
				
    var zip = new AdmZip(filePath);
    var zipEntries = zip.getEntries(); // an array of ZipEntry records
		
		var extractZipEntry = function(zipEntry) {
			if(isValidEntry(zipEntry)) {
				zip.extractEntryTo(zipEntry, destinationPath, true, true);
				console.log('Extract',zipEntry.name,'to',destinationPath);
				paths[zipEntry.name] = destinationPath + '/' + zipEntry.name;
			}
		};
		
		zipEntries.forEach(extractZipEntry);
		
		return paths;
	}
	
};

module.exports = FileHelper;