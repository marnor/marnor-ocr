var _ = require('lodash');
var fs = require('fs');
var Promise = require('bluebird');
var rimraf = require('rimraf');
var path = require('path');
var tesseract = require('node-tesseract');
var FileHelper = require('./file-helper');


var OCRService = function(options) {
  options = _.isObject(options) ? options : {};
  this.defaultOptions = {l:'swe', psm: 3};
  console.info("OPTIONS", options);
  this.options = _.extend({}, this.defaultOptions, options);
  this.tesseract = tesseract;
  console.log("-- OCR Service initialized with options --", this.options);
}

OCRService.prototype.zipToText = zipToText;
OCRService.prototype.imagesToText = imagesToText;
OCRService.prototype.imageToText = imageToText;

module.exports = OCRService;


/////////////////


function zipToText(pathToZip) {
  var self = this;
  var destination = 'temp/unzipped/' + Date.now();
	
  return FileHelper.createDir(destination)
    .then(() => {
      console.log("Unzipping",pathToZip,"to",destination);
      return FileHelper.unzip(pathToZip, destination);
    })
    .then(self.imagesToText)
    .finally(() => { rimraf(destination); });
}

function imagesToText(filePaths, options) {
  if(!_.isObject(filePaths)) {
    throw Error("filePaths argument must be an object! Was of type: " + (typeof filePaths));
  }
  var self = this;  
  var imageTypes = Object.keys(filePaths);
  options = _.isObject(options) ? _.extend({}, self.defaultOptions, options) : self.options;

  return Promise.map(_.toArray(filePaths), filePath => {
      return imageToText(filePath, options);
    })
    .then(texts => {
      return _.reduce(texts, (result, text, index) => {      
        var imageType = imageTypes[index];
        var filename = imageType.substring(0, imageType.lastIndexOf('-'));
        result[filename] = text;
        return result;
      }, {});
  });
}

function imageToText(filePath, options) {  
  var processFile = Promise.promisify(tesseract.process);
  return processFile(filePath, options).then(deleteFile(filePath));
}

function deleteFile(filePath) {
  return (text) => {    
      fs.unlink(filePath);
      return text;
  }
}